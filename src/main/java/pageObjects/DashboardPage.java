package pageObjects;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {

    public DashboardPage(AndroidDriver< AndroidElement > androidDriver){
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver),this);
    }

    @AndroidFindBy(id="com.mastercard.dis.mids.referenceapp:id/btn_create_id")
    WebElement createBtn;

    public void setCreateBtn(){
        createBtn.click();
    }

    @AndroidFindBy(xpath = "android.widget.Toast[1]")
    WebElement toastMsg;

    public String getToastMsg() {
        return toastMsg.getText();
    }
}
