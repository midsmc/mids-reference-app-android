package pageObjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class IntroLivenessPage {

    public IntroLivenessPage(AndroidDriver<AndroidElement> androidDriver) {
        PageFactory.initElements(new AppiumFieldDecorator(androidDriver),this);
    }
    @AndroidFindBy(id = "com.mastercard.dis.mids.referenceapp:id/btn_liveness_intro_continue")
    WebElement continueBtn;

    public void getContinueBtn(){
      continueBtn.click();
    }
    @AndroidFindBy(id = "com.mastercard.dis.mids.referenceapp:id/tv_liveness_tips_title")
    WebElement livenessTipsGetTitle;

    public String livenessTipsGetTitle(){
        return livenessTipsGetTitle.getText();
    }

    @AndroidFindBy(id="com.mastercard.dis.mids.referenceapp:id/btn_liveness_tips_got_it")
    WebElement livenessScanFace;

    public void getLivenessScanFace() {
        livenessScanFace.click();
    }

    @AndroidFindBy(id="com.android.permissioncontroller:id/permission_allow_foreground_only_button")
    WebElement cameraAccess;

    public WebElement getCameraAccess() {
        return cameraAccess;
    }
}
