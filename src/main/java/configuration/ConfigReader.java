package configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

    Properties pro;

    public ConfigReader() throws IOException {
        pro=new Properties();
        FileInputStream fileInputStream=new FileInputStream("src/main/resources/global.properties");
        pro.load(fileInputStream);
    }

    public String getApp(){
        String appName=pro.getProperty("app");
        if(appName !=null) return appName;
        else throw new RuntimeException("app is not present in configuration file");
    }
    public String device(){
        String device=pro.getProperty("device");
        if(device !=null) return device;
        else throw new RuntimeException("device is not present in configuration file");
    }
}
