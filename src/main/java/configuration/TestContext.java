package configuration;

import managers.AndroidDriverManager;
import managers.PageObjectManager;

import java.io.IOException;
import java.net.MalformedURLException;

public class TestContext {

    private AndroidDriverManager androidDriverManager;
    private PageObjectManager pageObjectManager;

    public TestContext() throws IOException, InterruptedException {
        androidDriverManager=new AndroidDriverManager();
        pageObjectManager=new PageObjectManager(androidDriverManager.createDriver());
    }

    public AndroidDriverManager getAndroidDriverManager(){
        return androidDriverManager;
    }
    public PageObjectManager getPageObjectManager(){
        return pageObjectManager;
    }
}
