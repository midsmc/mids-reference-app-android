package managers;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import pageObjects.DashboardPage;
import pageObjects.IntroLivenessPage;

import java.net.MalformedURLException;

public class PageObjectManager {

    private AndroidDriver<AndroidElement>androidDriver;
    private IntroLivenessPage introLivenessPage;
    private DashboardPage dashboardPage;

    public PageObjectManager(AndroidDriver<AndroidElement> androidDriver){
        this.androidDriver=androidDriver;
    }
    public IntroLivenessPage getIntroLivenessPage() throws MalformedURLException {
        return (introLivenessPage==null) ? new IntroLivenessPage(androidDriver):introLivenessPage;
    }

    public DashboardPage getDashboardPage() throws MalformedURLException {
        return (dashboardPage==null) ? new DashboardPage(androidDriver):dashboardPage;
    }
}
