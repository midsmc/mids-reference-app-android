package managers;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AndroidDriverManager {
    private DesiredCapabilities capabilities;
    private String app;
    private AndroidDriver<AndroidElement> androidDriver;
    private AppiumDriverLocalService service;
    private String device;

    public AndroidDriverManager() throws IOException {
        device=FileReaderManager.getInstance().getConfigReader().device();
        app=FileReaderManager.getInstance().getConfigReader().getApp();
    }


    public AndroidDriver<AndroidElement> createDriver() throws IOException, InterruptedException {
        File appDir=new File("src/main");
        File appPath=new File(appDir,app);
        capabilities = new DesiredCapabilities();
        if(device.equalsIgnoreCase("emulator")) {

            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator");
        }
        capabilities.setCapability(MobileCapabilityType.APP, appPath.getAbsolutePath());
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        androidDriver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        androidDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        return androidDriver;
    }

    public TouchAction touchAction(){
        return new TouchAction(androidDriver);
    }

}
