Feature: User able to load the app on the emulator and landed on the Liveness Intro Screen

  Scenario: User landed on the Liveness Intro Screen
    When user open app and click on continue button
    Then user landed on liveness scan face screen
    Then user click on Got it and get camera access

