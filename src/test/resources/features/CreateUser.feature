Feature: User able to load the app on the emulator and create a user id

  Scenario: User is created and retrieve id of user
    When user open app and click on create button
     And user register the created id with MIDS
    Then user get the user id
    Then user is successfully created with unique id