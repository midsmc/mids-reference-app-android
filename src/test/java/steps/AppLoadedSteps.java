package steps;

import configuration.TestContext;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.IntroLivenessPage;
import java.net.MalformedURLException;
import java.time.Duration;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class AppLoadedSteps {
 private static AndroidDriver<AndroidElement> androidDriver;
 private TestContext testContext;
 private IntroLivenessPage introLivenessPage;
 private TouchAction touchAction;

 public AppLoadedSteps(TestContext context) throws MalformedURLException {
  testContext=context;
  introLivenessPage=testContext.getPageObjectManager().getIntroLivenessPage();
  touchAction=testContext.getAndroidDriverManager().touchAction();
 }

 @When("user open app and click on continue button")
 public void userOpenAppAndClickOnContinueButton() {
  introLivenessPage.getContinueBtn();
 }

 @Then("user landed on liveness scan face screen")
 public void userLandedOnLivenessScanFaceScreen() {
  Assert.assertEquals(introLivenessPage.livenessTipsGetTitle(),"How to scan your face");
 }

 @Then("user click on Got it and get camera access")
 public void userClickOnGotItAndGetCameraAccess() {
  introLivenessPage.getLivenessScanFace();
  touchAction.longPress(longPressOptions().withElement(element(introLivenessPage.getCameraAccess())).withDuration(Duration.ofSeconds(2))).release().perform();


 }
}
