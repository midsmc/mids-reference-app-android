package steps;

import configuration.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.DashboardPage;

import java.net.MalformedURLException;

public class CreateUserSteps {

    TestContext testContext;
    DashboardPage dashboardPage;

    public CreateUserSteps(TestContext context) throws MalformedURLException {
        testContext=context;
        dashboardPage=context.getPageObjectManager().getDashboardPage();
    }

    @When("user open app and click on create button")
    public void user_open_app_and_click_on_create_button() {
        dashboardPage.setCreateBtn();
        System.out.println(dashboardPage.getToastMsg());
    }
    @When("user register the created id with MIDS")
    public void user_register_the_created_id_with_MIDS() {

    }
    @Then("user get the user id")
    public void user_get_the_user_id() {

    }
    @Then("user is successfully created with unique id")
    public void user_is_successfully_created_with_unique_id() {

    }
}
